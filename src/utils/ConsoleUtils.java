package utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class ConsoleUtils {

    private ConsoleUtils(){}


    public static void printMap(Map<String, Integer> map){
        for (Map.Entry<String, Integer> it : map.entrySet()){
            System.out.printf("%s : %s\n", it.getKey(), it.getValue());
        }
    }

    public static void printList(List<String> list){
        list.forEach(System.out::println);
    }

    public static List<String> readConsoleLines(int numberOfLines){
        List<String> result = new ArrayList<>();
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in)))
        {
            while(result.size() < numberOfLines){
                result.add(consoleReader.readLine());
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return result;
    }
}
