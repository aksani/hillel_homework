package utils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ArrayUtils {

    private ArrayUtils(){}

    /**
     * Sorts int array from min to max value.
     * @param array source array.
     * @return sorted array.
     */
    public static int [] sort(int[] array){
        for (int i = 0; i < array.length; i++){
            for (int j = i; j < array.length; j++){
                if(array[i] > array[j]){
                    int a = array[j];
                    array[j] = array[i];
                    array[i] = a;
                }
            }
        }
        return array;
    }

    /**
     * Sorts list of strings in alphabetical order.
     * @param listOfWords list of strings to be sorted.
     * @return sorted list.
     */
    public static List<String> sort(List<String> listOfWords){
        for (int i = 0; i < listOfWords.size(); i ++){
            for (int j = i; j < listOfWords.size(); j ++){
                char[] first = listOfWords.get(i).toCharArray();
                char[] second = listOfWords.get(j).toCharArray();

                for (int k = 0; (k < first.length && k < second.length); k++){
                    if (first[k] != second[k]){
                        if (first[k] > second[k]) {
                            String a = listOfWords.get(i);
                            listOfWords.set(i, listOfWords.get(j));
                            listOfWords.set(j, a);
                        }
                        break;
                    }
                    if ((second.length - 1) == k){
                        String a = listOfWords.get(i);
                        listOfWords.set(i, listOfWords.get(j));
                        listOfWords.set(j, a);
                    }
                }
            }
        }
        return listOfWords;
    }

    public static int binarySearch(int[] array, int value){
        int minPos = 0;
        int maxPos = array.length - 1;

        while (minPos <= maxPos){
            int middlePos = (minPos + maxPos)/2;

            if (value != array[middlePos]){
                if (value > array[middlePos]){
                    minPos = middlePos + 1;
                } else {
                    maxPos = middlePos - 1;
                }
            } else {
                return middlePos;
            }
        }
        return -1;
    }


    /**
     * Create list of words from list of text lines.
     * @param listOfLines list of text lines.
     * @return list of words.
     */
    public static List<String> wordsFromLines(List<String> listOfLines){
        List<String> result = new ArrayList<>();
        for (String line : listOfLines){
            result.addAll(Arrays.asList(line.split("[\\W]+")));
        }
        return result;
    }
}
