package utils;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public final class FileUtils {

    private FileUtils(){}


    /**
     * @param filePath string path to file.
     * @return list of lines from file.
     */
    public static List<String> readFile(String filePath){
        List<String> lines = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath)))
        {
            while (reader.ready()) {
                lines.add(reader.readLine());
            }
        } catch (IOException e){
            System.out.println("%Could not find file '" + filePath + "'!");
        }
        return lines;
    }


    /**
     * Writes list of strings to file.
     * @param filePath path of the file to be written.
     * @param list source list.
     * @param delimiter delimiter between list elements.
     */
    public static void writeFile(String filePath, List<String> list, String delimiter){
        File targetFile = new File(filePath);
        try (FileWriter fileWriter = new FileWriter(targetFile)) {
            if (!targetFile.exists()) {
                targetFile.createNewFile();
            }
            for (String word : list) {
                fileWriter.write(word + delimiter);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static boolean isFileContainsString(File file, String value){
        boolean result = false;
        try (BufferedReader reader = new BufferedReader(new FileReader(file)))
        {
            while (reader.ready()) {
                if (reader.readLine().contains(value)){
                    result = true;
                    break;
                }
            }
        } catch (IOException e){
            System.out.println("%Could not find file '" + file.getPath() + "'!");
        }
        return result;
    }

}
