package constants;


import java.util.Arrays;
import java.util.List;

public class StarDigitConst {

    private static final String[] ZERO = {"  ***  ", " *   * ", " *   * ", " *   * "," *   * ", " *   * ","  ***  "};
    private static final String[] ONE = {"   *  ", " * *  ", "   *  ", "   *  ", "   *  ", "   *  ", "  *** "};
    private static final String[] TWO = {"  ***  ", " *   * ", "    *  ", "   *   ", "  *    ", " *     ", " ***** "};
    private static final String[] THREE = {"  ***  ", " *   * ", "    *  ", "   *   ", "     * ", " *   * ", "  ***  "};
    private static final String[] FOUR = {" *   * ", " *   * ", " *   * ", " ***** ", "     * ", "     * ", "     * "};
    private static final String[] FIVE = {" ***** ", " *     ", " *     ", "  ***  ", "     * ", " *   * ", "  ***  "};
    private static final String[] SIX = {"  **** ", " *     ", " *     ", " ****  ", " *   * ", " *   * ", "  ***  "};
    private static final String[] SEVEN = {" ***** ", "     * ", "     * ", "    *  ", "   *   ", "  *    ", " *     "};
    private static final String[] EIGHT = {"  ***  ", " *   * ", " *   * ", "  ***  ", " *   * ", " *   * ", "  ***  "};
    private static final String[] NINE = {"  ***  ", " *   * ", " *   * ", "  **** ", "     * ", " *   * ", "  ***  "};

    public static List<String[]> getListOfStarDigits(){
        return Arrays.asList(ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE);
    }
}
