package tasks;

import constants.StarDigitConst;
import utils.ConsoleUtils;

/**
 * Task:
 * Print asterisks representation of given number to console.
 * 01.12.17
 */
public class StarDigits {

    public static void main(String[] args) {
        String consoleLine = ConsoleUtils.readConsoleLines(1).get(0);

        try {
            for (int i = 0; i < 7; i++) {
                StringBuilder result = new StringBuilder();
                for (char digit : consoleLine.toCharArray()) {
                    int intDigit = Integer.parseInt(String.valueOf(digit));
                    result.append(StarDigitConst.getListOfStarDigits().get(intDigit)[i]);

                }
                System.out.println(result);

            }
        } catch (NumberFormatException e){
            System.out.println("This is not a number!");
        }
    }
}
