package tasks;

import utils.FileUtils;

import java.util.List;

/**
 * Task:
 * Delete all words from file which contain digits.
 * 29.11.17
 */
public class NoDigits {

    public static void main(String[] args) {
        List<String> fileLines = FileUtils.readFile("werther.txt");
        for (int i = 0; i < fileLines.size(); i++){
            String[] wordsFromLine = fileLines.get(i).split("[\\W]+");
            for (String word : wordsFromLine) {
                char[] wordCharArr = word.toCharArray();
                for (char wordChar : wordCharArr) {
                    if (Character.isDigit(wordChar)) {
                        fileLines.set(i, fileLines.get(i).replace(word, ""));
                    }
                }
            }

        }

        FileUtils.writeFile("werther.txt", fileLines, "\n");
    }
}
