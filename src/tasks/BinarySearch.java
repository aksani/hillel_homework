package tasks;

import utils.ArrayUtils;

/**
 * Task:
 * Binary search in array.
 * 23.11.17
 */
public class BinarySearch {



    public static void main(String[] args) {
        int[] array = new int[]{2,6,44,78,99,3,40,42};
        array = ArrayUtils.sort(array);
        System.out.println(ArrayUtils.binarySearch(array, 40));
    }
}
