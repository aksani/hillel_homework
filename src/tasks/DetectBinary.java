package tasks;

import utils.ConsoleUtils;

import java.util.List;

/**
 * Task:
 * Detect whether number is binary.
 * 01.12.17
 */
public class DetectBinary {

    public static void main(String[] args) {
        List<String> consoleInput = ConsoleUtils.readConsoleLines(1);
        long number = 0;
        try {
            number = Long.parseLong(consoleInput.get(0));
        } catch (NumberFormatException e){
            System.out.println("This is not a number!");
        }

        if (number != 0){
           for (char digit : consoleInput.get(0).toCharArray()){
               if (Integer.parseInt(String.valueOf(digit)) < 2){
                   System.out.println("This number is not a binary");
                   System.exit(0);
               }
           }
            System.out.println("This number is a binary");
        }

    }
}
