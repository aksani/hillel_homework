package tasks;

import utils.ArrayUtils;
import utils.FileUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Task:
 * Count all repetitions of words from file.
 * 23.11.17
 */
public class RepeatCounter {

    public static void main(String[] args) {

        List<String> listOfWords = ArrayUtils.wordsFromLines(FileUtils.readFile("werther.txt"));

        Map<String, Long> resultMap = listOfWords.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));

        resultMap.forEach((key, value) -> System.out.println(key + " : " + value));
    }


}
