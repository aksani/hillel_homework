package tasks;

import utils.ArrayUtils;
import utils.FileUtils;

import java.util.List;

/**
 * Task:
 * Sort strings from one file to another in alphabetical order.
 * 27.11.17
 */
public class SortStrings {

    public static void main(String[] args) {
        List<String> words = ArrayUtils.wordsFromLines(FileUtils.readFile("werther.txt"));
        FileUtils.writeFile("result.txt", ArrayUtils.sort(words), "\n");
    }

}
