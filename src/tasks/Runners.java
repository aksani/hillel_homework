package tasks;

/**
 * Task:
 * From given arrays of runners names and their time results,
 * find the quickest runner and the second after the winner.
 * 27.11.17
 */
public class Runners {

    public static void main(String[] args) {
        String[] names = new String[]{"John","Marta","Vasil","Petro","Denys"};
        int[] times = new int[]{10, 5, 86, 33, 2};

        int firstIndex = 0;
        int secondIndex = 1;
        for(int i = 1; i < times.length; i++){
            if (times[firstIndex] > times[i]) {
                secondIndex = firstIndex;
                firstIndex = i;
            } else {
                if (times[secondIndex] > times[i]) {
                    secondIndex = i;
                }
            }
        }
        System.out.printf("First runner: %s.\nSecond runner: %s.", names[firstIndex], names[secondIndex]);
    }
}
