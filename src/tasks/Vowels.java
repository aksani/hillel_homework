package tasks;

import utils.ArrayUtils;
import utils.ConsoleUtils;
import utils.FileUtils;

import java.util.*;

/**
 * Task:
 * Print all words contain max number of vowels in alphabetical order.
 * Find all words contain at least 2 'a' letters.
 * 29.11.17
 */
public class Vowels {

    private static final char[] vowels = {'a', 'e', 'i', 'o', 'u', 'y'};

    public static void main(String[] args) {

        Map<String, Integer> counter = new HashMap<>();
        Map<String, Integer> wordsContainA = new HashMap<>();
        List<String> words = ArrayUtils.wordsFromLines(FileUtils.readFile("werther.txt"));

        //Count number of vowels and 'a' letters
        for (String word : words){
            if (!counter.containsKey(word)) {
                char[] wordChars = word.toLowerCase().toCharArray();
                for (char wordChar : wordChars) {
                    for (char vowel : vowels) {
                        if (wordChar == vowel) {
                            if (wordChar == 'a'){
                                int i = (wordsContainA.get(word) != null) ? wordsContainA.get(word) : 0;
                                wordsContainA.put(word, ++i);
                            }
                            int i = (counter.get(word) != null) ? counter.get(word) : 0;
                            counter.put(word, ++i);
                            break;
                        }
                    }
                }
            }
        }

        //Filter maps to meet conditions
        int max = 0;
        for (Map.Entry<String, Integer> i : counter.entrySet()){
            if (max <= i.getValue()) {
                max = i.getValue();
            }
        }
        for (Map.Entry<String, Integer> i : new HashMap<>(counter).entrySet()){
            if (max > i.getValue()) {
                counter.remove(i.getKey());
            }
        }

        new HashMap<>(wordsContainA).forEach((key, value) -> {if (value < 2) wordsContainA.remove(key);});

        //Sort and print result
        System.out.printf("Words with max %d number of vowels:\n", max);
        ConsoleUtils.printList(ArrayUtils.sort(new ArrayList<>(counter.keySet())));

        System.out.println("Words contain at least 2 'a' letters:");
        ConsoleUtils.printList(new ArrayList<>(wordsContainA.keySet()));
    }
}
