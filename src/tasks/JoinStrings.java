package tasks;

import utils.ConsoleUtils;
import utils.FileUtils;

import java.io.File;
import java.util.List;

/**
 * Task:
 * From given 3 strings create a word from dictionary if it's possible.
 * Print result words.
 * 29.11.17
 */
public class JoinStrings {

    private final static File dictionary = new File("dictionary.txt");

    public static void main(String[] args) {

        List<String> strings = ConsoleUtils.readConsoleLines(3);

        for (int i = 0; i < strings.size(); i++){
            strings.add(0, strings.get(strings.size() - 1));
            strings.remove(strings.size() - 1);
            String word = String.join("", strings);
            if (FileUtils.isFileContainsString(dictionary, word)){
                System.out.println(word);
            }
        }
    }
}
