package tasks;

import utils.ConsoleUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Task:
 * Compress string by replacing repetitive letters with number of repetition.
 * If the result string is not less then first, print the first variant.
 * 01.12.17
 */
public class CompactString {

    public static void main(String[] args) {
        String consoleString = ConsoleUtils.readConsoleLines(1).get(0);
        String resultString = compressString(consoleString);

        if (consoleString.length() <= resultString.length()){
            System.out.println(consoleString);
        } else
            System.out.println(resultString);
    }




    private static String compressString(String value){

        Map<String, Integer> result = new LinkedHashMap<>();

        for (char c : value.toCharArray()){
            String letter = String.valueOf(c);
            int i = (result.get(letter) != null) ? result.get(letter) : 0;
            result. put(letter, ++i);
        }
        String stringResult = "";
        for (Map.Entry<String, Integer> it : result.entrySet()) {
            stringResult += it.getKey() + it.getValue();
        }

        return stringResult;
    }
}
