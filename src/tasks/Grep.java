package tasks;

import utils.FileUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Task:
 * Console application which displays all lines that match specified arguments.
 * Similar to grep tool in Unix-based systems.
 * 24.11.17
 */
public class Grep {

    public static void main(String[] args) {
        String consoleLine;
        System.out.println("Welcome to console application Grep 2.0!");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            while (true){
                System.out.print(">");
                if ((consoleLine = reader.readLine()).isEmpty()) {
                    continue;
                }
                if (consoleLine.equals("exit")){
                    break;
                }
                if (consoleLine.split(" \\| ").length < 2){
                    System.out.println("%Wrong number of arguments!\n[FILE_NAME] | [[ARG1] [ARG2] [ARG3] etc] or [REGEX]");
                    continue;
                }
                String[] lineArr = consoleLine.split(" \\| ");
                List<String> fileLines = FileUtils.readFile(lineArr[0]);
                if (fileLines.isEmpty()){
                    continue;
                }
                List<String> argums = Arrays.asList(lineArr[1].split(" "));
                printFilterLines(fileLines, argums);
            }

        } catch (IOException e){e.printStackTrace();}

    }



    private static void printFilterLines(List<String> lines, List<String> argums){
        String regex = "(?i)^.*\\b" + argums.toString().replace(", ", "|")
                .replace("[", "(").replace("]", ")") + "\\b.*$";

        Pattern pattern = Pattern.compile(regex);
        for (String fileLine : lines) {
            Matcher matcher = pattern.matcher(fileLine);
            if (matcher.find()){
                System.out.println(fileLine);
            }
        }
    }
}
