package tasks;

import utils.ConsoleUtils;


/**
 * Task:
 * Determine whether entered number is palindrome.
 * 01.12.17
 */
public class Palindromes {

    public static void main(String[] args) {

        String consoleLine = ConsoleUtils.readConsoleLines(1).get(0);
        try {
            int number = Integer.parseInt(consoleLine);
            if (isPalindrome(number)){
                System.out.println("The number is palindrome!");
            } else {
                System.out.println("The number is not palindrome!");
            }
        } catch (NumberFormatException e){
            System.out.println("This is not a number!");
        }

    }


    private static boolean isPalindrome(int number){
        int regularNumber = number;
        int reverseNumber = 0;

        while (regularNumber != 0){
            int a = regularNumber % 10;
            reverseNumber = reverseNumber * 10 + a;
            regularNumber = regularNumber / 10;
        }

        return number == reverseNumber;
    }

}
